---
title: "A Day in the Winter"
date: 2020-10-29T18:39:51+05:30
draft: false
disqus: false
type: posts
description: "A day in January when it all started"
---

It all started in the month on January. The day cold as its usually is in January winter. It was a Saturday morning. I was sitting there siping a hot cup of Mocha Tea off my China tea cup that I had recently bought on Amazon. Today was cloudy. A bit chilly than other Saturday mornings that I have experienced. I could feel the gust of dry cold wind all over my face. This weekend was big. The next day was Republic day. Exactly 70 years ago, on this day the Constitution of India came into effect. India identified herself as democracy, the right and duties of citizens were defined well in this very book. India sworn its first President on this day.




Its a National holiday for the citizens (not this year though, it was a Sunday). Indian Army showcases its marches and parades. Its a huge event. Foreign dignitories, media every one is looking up to the PM for his inaugral speech. There is also a Chief Guest of Honour invited to join the PM in the celebrations. Let me tell you that the chief guest at Republic Day parade is accorded the highest honour in terms of protocol. A lot of people, across many nationals and diverse races had been hounoured to sit as Chief Guest over the years. The President of Brazil was provided with this Honour this year.



There was an air of unwillingness surrounding the selection of Chief Guest though. I had seen numerous accounts of users on Twitter calling it a mis-judjement. There were a few trending tweets which set the notion that the Ministry of External Affairs fell short this time to honour a possible figure as a Guest. I was there browsing though the news articles to see what I was missing and what has let to al this. Apparantly the invitation is send 6 months in advance. So they definitly knew what thy were up to.



While digging up something on the process of selection of this Guest of Honour and the flak that this decision of receiving, I got tied up in multiple news articles from several media outlets spanning entire world. While I was on it, I stumbled across a few news articles about some different sort of health conditions that was being reported by citizens of a specific province in China. All the reports mentioned a pnuemonia like symptom. This had spiralled out of hands in China and some EU countries were also reporting such concerns at large. By the last week of January there were thousands of active and the toll had reached hundreds.



The scientific community had isolated and started to test the disease causing pathogen. They confirmed it was a corona-shaped virus that resembled and belonged to the SARS(2003) family of viruses. Soon after this WHO(World Health Organization) declared this an epidemic. After a few weeks when the toll has reached thousands did WHO again updated its stance and further called this virus deadly and an COVID a epidemic. This was spreading so fast that very country had to go into lockdown. In India that day was March 25th.



That was the day I took some time out to ponder upon. Or you can say my musings. It has finally come down to this. Writing down my first blog. Shortly after this I will write one more post about the tools and stack used to write this blog. 



Till then stay safe. Stay at home. Wear a mask.

